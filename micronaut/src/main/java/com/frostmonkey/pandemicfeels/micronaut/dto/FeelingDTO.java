package com.frostmonkey.pandemicfeels.micronaut.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public class FeelingDTO {
    private LocalDateTime feelingDate;
    private String comment;
    private Integer feelingScore;
    private UUID feeler;

    public LocalDateTime getFeelingDate() {
        return feelingDate;
    }

    public void setFeelingDate(LocalDateTime feelingDate) {
        this.feelingDate = feelingDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getFeelingScore() {
        return feelingScore;
    }

    public void setFeelingScore(Integer feelingScore) {
        this.feelingScore = feelingScore;
    }

    public UUID getFeeler() {
        return feeler;
    }

    public void setFeeler(UUID feeler) {
        this.feeler = feeler;
    }
}
