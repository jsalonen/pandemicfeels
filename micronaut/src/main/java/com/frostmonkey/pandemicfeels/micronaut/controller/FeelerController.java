package com.frostmonkey.pandemicfeels.micronaut.controller;

import com.frostmonkey.pandemicfeels.micronaut.dto.FeelerDTO;
import com.frostmonkey.pandemicfeels.micronaut.dto.FeelingDTO;
import com.frostmonkey.pandemicfeels.micronaut.service.FeelerService;
import com.frostmonkey.pandemicfeels.micronaut.service.FeelingService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.*;
import io.micronaut.http.hateoas.JsonError;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Controller("/feelers")
public class FeelerController {

    @Inject
    FeelerService feelerService;

    @Inject
    FeelingService feelingService;

    @Post
    public HttpResponse<UUID> createFeeler(@Body FeelerDTO feeler) {
        UUID created = feelerService.createFeeler(feeler);
        return HttpResponse.created(created);
    }

    @Get("/{uuid}")
    public HttpResponse<FeelerDTO> getFeeler(@PathVariable UUID uuid) {
        FeelerDTO feeler = feelerService.getFeeler(uuid);
        return HttpResponse.ok(feeler);
    }

    @Patch("/{uuid}")
    public HttpResponse<FeelerDTO> updateFeeler(@PathVariable UUID uuid, @Body FeelerDTO feeler) {
        FeelerDTO updated = feelerService.updateFeeler(uuid, feeler);
        return HttpResponse.ok(updated);
    }

    @Get("/{uuid}/feelings")
    public HttpResponse<List<FeelingDTO>> getFeelerFeelings(@PathVariable UUID uuid) {
        List<FeelingDTO> feelings = feelingService.getFeelerFeelings(uuid);
        return HttpResponse.ok(feelings);
    }

    @Error
    public HttpResponse<JsonError> notFoundError(HttpRequest request, EntityNotFoundException notFoundException) {
        JsonError error = new JsonError("Feeler not found");
        return HttpResponse.<JsonError>status(HttpStatus.NOT_FOUND, "Not found").body(error);
    }
}
