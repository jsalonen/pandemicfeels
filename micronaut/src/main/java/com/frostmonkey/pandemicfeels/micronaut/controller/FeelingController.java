package com.frostmonkey.pandemicfeels.micronaut.controller;

import com.frostmonkey.pandemicfeels.micronaut.dto.FeelingDTO;
import com.frostmonkey.pandemicfeels.micronaut.service.FeelerFeelingMismatchException;
import com.frostmonkey.pandemicfeels.micronaut.service.FeelingService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.hateoas.JsonError;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Controller("/feelings")
public class FeelingController {

    @Inject
    FeelingService feelingService;

    @Post
    public HttpResponse<UUID> createFeeling(@Body FeelingDTO feeling) throws FeelerFeelingMismatchException {
        UUID created = feelingService.createFeeling(feeling);
        return HttpResponse.created(created);
    }

    @Get("/{uuid}")
    public HttpResponse<FeelingDTO> getFeeling(@PathVariable UUID uuid) {
        FeelingDTO feeling = feelingService.getFeeling(uuid);
        return HttpResponse.ok(feeling);
    }

    @Patch("/{uuid}")
    public HttpResponse<FeelingDTO> updateFeeling(@PathVariable UUID uuid, @Body FeelingDTO feeling) {
        FeelingDTO updated = feelingService.updateFeeling(uuid, feeling);
        return HttpResponse.ok(updated);
    }

    @Error
    public HttpResponse<JsonError> notFoundError(HttpRequest request, EntityNotFoundException notFoundException) {
        JsonError error = new JsonError("Feeling not found");
        return HttpResponse.<JsonError>status(HttpStatus.NOT_FOUND, "Not found").body(error);
    }

    public HttpResponse<JsonError> feelerFeelingMismatch(HttpRequest request, FeelerFeelingMismatchException exception) {
        JsonError error = new JsonError("Feeler and feeling do not match");
        return HttpResponse.<JsonError>status(HttpStatus.BAD_REQUEST, "Oi, you got a loicence for that change?").body(error);
    }
}
