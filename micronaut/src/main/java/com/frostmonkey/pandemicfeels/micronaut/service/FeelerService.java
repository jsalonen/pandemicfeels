package com.frostmonkey.pandemicfeels.micronaut.service;

import com.frostmonkey.pandemicfeels.micronaut.dto.FeelerDTO;
import com.frostmonkey.pandemicfeels.micronaut.dto.FeelingDTO;
import com.frostmonkey.pandemicfeels.micronaut.repository.Feeler;
import com.frostmonkey.pandemicfeels.micronaut.repository.FeelerRepository;
import com.frostmonkey.pandemicfeels.micronaut.repository.Feeling;
import com.frostmonkey.pandemicfeels.micronaut.repository.FeelingRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class FeelerService {

    @Inject
    FeelerRepository repository;

    public UUID createFeeler(FeelerDTO dto) {
        Feeler feeler = dtoToEntity(dto);
        Feeler created = repository.save(feeler);
        return created.getId();
    }

    public FeelerDTO getFeeler(UUID uuid) {
        Optional<Feeler> feeler = repository.findById(uuid);
        if (feeler.isPresent()) {
            FeelerDTO dto = entityToDTO(feeler.get());
            return dto;
        } else {
            throw new EntityNotFoundException();
        }
    }

    public FeelerDTO updateFeeler(UUID uuid, FeelerDTO dto) {
        Optional<Feeler> original = repository.findById(uuid);
        if (original.isPresent()) {
            Feeler feeler = mergeUpdateEntity(original.get(), dto);
            Feeler updated = repository.update(feeler);
            return entityToDTO(updated);
        } else {
            throw new EntityNotFoundException();
        }
    }

    private Feeler dtoToEntity(FeelerDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Feeler feeler = modelMapper.map(dto, Feeler.class);
        return feeler;
    }

    private Feeler mergeUpdateEntity(Feeler original, FeelerDTO dto) {
        if (null != dto.getUsername()) { original.setUsername(dto.getUsername()); }
        if (null != dto.getPassword()) { original.setPassword(dto.getPassword()); }
        if (null != dto.getEmail()) { original.setEmail(dto.getEmail()); }
        return original;
    }

    private FeelerDTO entityToDTO(Feeler feeler) {
        ModelMapper modelMapper = new ModelMapper();
        FeelerDTO dto = modelMapper.map(feeler, FeelerDTO.class);
        dto.setPassword(null);
        return dto;
    }
}
