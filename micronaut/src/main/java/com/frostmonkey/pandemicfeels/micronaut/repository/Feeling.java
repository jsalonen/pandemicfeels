package com.frostmonkey.pandemicfeels.micronaut.repository;

import io.micronaut.data.annotation.DateCreated;
import io.micronaut.data.annotation.DateUpdated;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Feeling {
    @Id
    @GeneratedValue
    private UUID id;

    private LocalDateTime feelingDate;
    private String comment;
    private Integer feelingScore;

    @ManyToOne
    private Feeler feeler;

    @DateCreated
    private LocalDateTime created;

    @DateUpdated
    private LocalDateTime updated;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getFeelingDate() {
        return feelingDate;
    }

    public void setFeelingDate(LocalDateTime feelingDate) {
        this.feelingDate = feelingDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getFeelingScore() {
        return feelingScore;
    }

    public void setFeelingScore(Integer feelingScore) {
        this.feelingScore = feelingScore;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public Feeler getFeeler() {
        return feeler;
    }

    public void setFeeler(Feeler feeler) {
        this.feeler = feeler;
    }
}
