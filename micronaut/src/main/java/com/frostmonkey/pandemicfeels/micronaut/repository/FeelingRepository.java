package com.frostmonkey.pandemicfeels.micronaut.repository;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

@Repository
public interface FeelingRepository extends CrudRepository<Feeling, UUID> {

    List<Feeling> findByFeelerId(UUID uuid);
}
