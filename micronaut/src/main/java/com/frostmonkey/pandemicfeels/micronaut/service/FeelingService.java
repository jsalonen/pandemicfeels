package com.frostmonkey.pandemicfeels.micronaut.service;

import com.frostmonkey.pandemicfeels.micronaut.dto.FeelingDTO;
import com.frostmonkey.pandemicfeels.micronaut.repository.Feeler;
import com.frostmonkey.pandemicfeels.micronaut.repository.FeelerRepository;
import com.frostmonkey.pandemicfeels.micronaut.repository.Feeling;
import com.frostmonkey.pandemicfeels.micronaut.repository.FeelingRepository;
import org.modelmapper.ModelMapper;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class FeelingService {

    @Inject
    FeelingRepository repository;

    @Inject
    FeelerRepository feelerRepository;

    public UUID createFeeling(FeelingDTO dto) throws FeelerFeelingMismatchException {
        Optional<Feeler> feeler = feelerRepository.findById(dto.getFeeler());
        if (feeler.isPresent()) {
            Feeling feeling = dtoToEntity(dto);
            feeling.setFeeler(feeler.get());
            Feeling created = repository.save(feeling);
            return created.getId();
        } else {
            throw new FeelerFeelingMismatchException();
        }
    }

    public FeelingDTO getFeeling(UUID uuid) {
        Optional<Feeling> feeling = repository.findById(uuid);
        if (feeling.isPresent()) {
            FeelingDTO dto = entityToDTO(feeling.get());
            return dto;
        } else {
            throw new EntityNotFoundException();
        }
    }

    public FeelingDTO updateFeeling(UUID uuid, FeelingDTO dto) {
        Optional<Feeling> original = repository.findById(uuid);
        if (original.isPresent()) {
            Feeling feeling = mergeUpdateEntity(original.get(), dto);
            Feeling updated = repository.update(feeling);
            return entityToDTO(updated);
        } else {
            throw new EntityNotFoundException();
        }
    }

    public List<FeelingDTO> getFeelerFeelings(UUID uuid) {
        List<Feeling> feelings = repository.findByFeelerId(uuid);
        List<FeelingDTO> dtos = feelings.stream().map(this::entityToDTO).collect(Collectors.toList());
        return dtos;
    }


    private Feeling dtoToEntity(FeelingDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Feeling feeler = modelMapper.map(dto, Feeling.class);
        return feeler;
    }

    private Feeling mergeUpdateEntity(Feeling original, FeelingDTO dto) {
        if (null != dto.getComment()) { original.setComment(dto.getComment()); }
        if (null != dto.getFeelingDate()) { original.setFeelingDate(dto.getFeelingDate()); }
        if (null != dto.getFeelingScore()) { original.setFeelingScore(dto.getFeelingScore()); }
        return original;
    }

    private FeelingDTO entityToDTO(Feeling feeler) {
        ModelMapper modelMapper = new ModelMapper();
        FeelingDTO dto = modelMapper.map(feeler, FeelingDTO.class);
        return dto;
    }
}
