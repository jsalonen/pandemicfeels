package com.frostmonkey.pandemicfeels.helidon.service;

import io.helidon.dbclient.DbClient;
import io.helidon.webserver.Routing;
import io.helidon.webserver.ServerRequest;
import io.helidon.webserver.ServerResponse;
import io.helidon.webserver.Service;

public class FeelingService implements Service {

    private final DbClient dbClient;

    public FeelingService(DbClient dbClient) {
        this.dbClient = dbClient;
    }

    @Override
    public void update(Routing.Rules rules) {
        rules
            .post("/", this::createFeeling)
            .get("/{uuid}", this::getFeeling)
            .patch("/{uuid}", this::updateFeeling);
    }

    private void updateFeeling(ServerRequest serverRequest, ServerResponse serverResponse) {

    }

    private void getFeeling(ServerRequest serverRequest, ServerResponse serverResponse) {

    }

    private void createFeeling(ServerRequest serverRequest, ServerResponse serverResponse) {

    }
}
